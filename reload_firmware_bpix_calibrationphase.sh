#!/usr/bin/env zsh


if [[ -n $1 ]]
then
    echo 'will ' $1 'firmware'
else
    echo 'to reload do:'
    echo './reload_firmware_bpix_calibration_phase.sh reload'
    echo 'to list do:'
    echo './reload_firmware_bpix_calibration_phase.sh list'
fi


# The tool to load firmware is in the pixelpilot  and pixeldev directory:
#cd /nfshome0/pixelpilot/FEDtool
echo ''
echo changing directory
echo cd /nfshome0/pixeldev/FEDtool
cd /nfshome0/pixeldev/FEDtool
echo ''
# Source the setup (on a srv machine)
echo sourcing setup script:
echo source setup.sh
source setup.sh

echo ''
echo will use firmware versions tkfec v44, pxfec v1.2.0, pixfed v15.3
echo from January 31, see https://indico.cern.ch/event/701102/contributions/2876173/attachments/1592921/2521620/DAQNews_310118.pdf
# firmware versions from Jan. 31:
# https://indico.cern.ch/event/701102/contributions/2876173/attachments/1592921/2521620/DAQNews_310118.pdf

echo ''
if [[ -n $1 ]]
then
    echo 'will ' $1 'firmware'
    if [[ $1 == 'reload' ]]
    then
        echo ''
        echo will reload firmware
        # TKFEC firmware v44
        # Load firmware for all TKFECs
        echo reloading tkfec firmware:
        echo bin/fpgaconfig  -c settings/FWUpload_BPIX_tkFECs.xml -i fec_V44_RARP.bit
        bin/fpgaconfig  -c settings/FWUpload_BPIX_tkFECs.xml -i fec_V44_RARP.bit

        echo ''
        # pxFEC firmware 1.2.0
        # Load firmware for all pixelFECs
        echo reloading pxfec firmware:
        echo bin/fpgaconfig  -c settings/FWUpload_BPIX_pxFECs.xml -i PFEC_V1.2.0.bin
        bin/fpgaconfig  -c settings/FWUpload_BPIX_pxFECs.xml -i PFEC_V1.2.0.bin

        echo ''
        # FED firmware pixfed_v15.3.bin
        # Load firmware for all FEDs
        echo reloading fed firmware:
        echo bin/fpgaconfig  -c settings/FWUpload_BPIX_FEDs.xml -i pixfed_v15.3.bin
        bin/fpgaconfig  -c settings/FWUpload_BPIX_FEDs.xml -i pixfed_v15.3.bin
    elif [[ $1 == 'list' ]]
    then
        # For help:
        # bin/fpgaconfig -h
        # For listing:
        echo ''
        echo 'listing firmware:'
        echo bin/fpgaconfig -c settings/FWUpload_BPIX_tkFECs.xml -l
        bin/fpgaconfig -c settings/FWUpload_BPIX_tkFECs.xml -l
        echo bin/fpgaconfig -c settings/FWUpload_BPIX_pxFECs.xml -l
        bin/fpgaconfig -c settings/FWUpload_BPIX_pxFECs.xml -l
        echo bin/fpgaconfig -c settings/FWUpload_BPIX_FEDs.xml   -l
        bin/fpgaconfig -c settings/FWUpload_BPIX_FEDs.xml   -l
    else
        echo ''
        echo will not list/reload firmware
        echo 'to list do:'
        echo './reload_firmware_bpix_calibration_phase.sh list'
        echo ''
        echo 'to reload do:'
        echo './reload_firmware_bpix_calibration_phase.sh reload'
        echo ''
        echo or use one of these lines:
        echo For help do
        echo bin/fpgaconfig -h
        echo ''

        echo 'listing firmware:'
        echo bin/fpgaconfig -c settings/FWUpload_BPIX_tkFECs.xml -l
        echo bin/fpgaconfig -c settings/FWUpload_BPIX_pxFECs.xml -l
        echo bin/fpgaconfig -c settings/FWUpload_BPIX_FEDs.xml   -l

        echo ''
        # TKFEC firmware v44
        # Load firmware for all TKFECs
        echo reloading tkfec firmware:
        echo bin/fpgaconfig  -c settings/FWUpload_BPIX_tkFECs.xml -i fec_V44_RARP.bit

        echo ''
        # pxFEC firmware 1.2.0
        # Load firmware for all pixelFECs
        echo reloading pxfec firmware:
        echo bin/fpgaconfig  -c settings/FWUpload_BPIX_pxFECs.xml -i PFEC_V1.2.0.bin

        echo ''
        # FED firmware pixfed_v15.3.bin
        # Load firmware for all FEDs
        echo reloading fed firmware:
        echo bin/fpgaconfig  -c settings/FWUpload_BPIX_FEDs.xml -i pixfed_v15.3.bin
    fi

else
    echo will not list/reload firmware
    echo 'to list do:'
    echo './reload_firmware_bpix_calibration_phase.sh list'
    echo ''
    echo 'to reload do:'
    echo './reload_firmware_bpix_calibration_phase.sh reload'

    echo ''
    echo or use one of these lines:
    echo For help do
    echo bin/fpgaconfig -h

    echo ''
    echo 'listing firmware:'
    echo bin/fpgaconfig -c settings/FWUpload_BPIX_tkFECs.xml -l
    echo bin/fpgaconfig -c settings/FWUpload_BPIX_pxFECs.xml -l
    echo bin/fpgaconfig -c settings/FWUpload_BPIX_FEDs.xml   -l

    echo ''
    # TKFEC firmware v44
    # Load firmware for all TKFECs
    echo reloading tkfec firmware:
    echo bin/fpgaconfig  -c settings/FWUpload_BPIX_tkFECs.xml -i fec_V44_RARP.bit

    echo ''
    # pxFEC firmware 1.2.0
    # Load firmware for all pixelFECs
    echo reloading pxfec firmware:
    echo bin/fpgaconfig  -c settings/FWUpload_BPIX_pxFECs.xml -i PFEC_V1.2.0.bin

    echo ''
    # FED firmware pixfed_v15.3.bin
    # Load firmware for all FEDs
    echo reloading fed firmware:
    echo bin/fpgaconfig  -c settings/FWUpload_BPIX_FEDs.xml -i pixfed_v15.3.bin
fi



