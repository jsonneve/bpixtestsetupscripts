#!/usr/bin/env zsh

sqlite3 /home/cmspixel/pixeldb/caliblog/cmspixel2.db < /home/cmspixel/pixeldb/caliblog/calibs.sql

echo '<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
  <title>calibrations</title>
  <style type="text/css">
    <!--
      body { background: white; color: #666f85; text-align: center }
      img  { border: none }
    -->
  </style>
</head>
<body>
<table>
' > /root/scrips/calibs.html
cat calibstable.html >> /root/scrips/calibs.html
echo "
</table>
</body>
</html>
" >> /root/scrips/calibs.html

