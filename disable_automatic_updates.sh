#!/bin/sh
################################
# Disable yum cron and daemons #
################################

echo "++++++++++++++++++++++++++++++++++++++++++++++++"
echo "Yum cron jobs are now disabled"

[ -e /etc/cron.hourly/yum-autoupdate ] && rm -f /etc/cron.hourly/yum-autoupdate
[ -e /etc/cron.hourly/yum ] && rm -f /etc/cron.hourly/yum
[ -e /etc/cron.daily/yum.cron ] && rm -f /etc/cron.daily/yum.cron

yum_status=`chkconfig --list |grep yum |grep "3:on"`
service_name=`chkconfig --list |grep yum | cut -f 1`

for serv in $service_name; do
  if [ "x$yum_status" != "x" ]; then
    echo "++++++++++++++++++++++++++++++++++++++++++++++++"
    echo "+ $serv has been switched to off"
    chkconfig $serv off
  fi

  if [ -e /var/lock/subsys/$serv ]; then
    /etc/init.d/$serv stop
  fi

  echo "Done $serv"
done
############################


# reaction from machine:
# root@cmspixel2 ~/scrips # ./disable_automatic_updates.sh 
# ++++++++++++++++++++++++++++++++++++++++++++++++
# Yum cron jobs are now disabled
# 
# Note: This output shows SysV services only and does not include native
#       systemd services. SysV configuration data might be overridden by native
#       systemd configuration.
# 
#       If you want to list systemd services use 'systemctl list-unit-files'.
#       To see services enabled on particular target use
#       'systemctl list-dependencies [target]'.
# 
# 
# Note: This output shows SysV services only and does not include native
#       systemd services. SysV configuration data might be overridden by native
#       systemd configuration.
# 
#       If you want to list systemd services use 'systemctl list-unit-files'.
#       To see services enabled on particular target use
#       'systemctl list-dependencies [target]'.
# 