#!/usr/bin/env zsh




for hostname in blancanieves cmspixel2 srv-s2b18-37-01
do
    sqlite3 /home/bpix/pixeldb/caliblog/${hostname}.db < /home/bpix/pixeldb/caliblog/calibs.sql

    echo '<?xml version="1.0" encoding="utf-8" ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
    <head>
      <title>calibrations</title>
      <style type="text/css">
        <!--
          body { background: white; color: #666f85; text-align: center }
          img  { border: none }
        -->
      </style>
    </head>
    <body>
    ' > /root/scrips/calibs${hostname}.html
    echo $hostname  >> /root/scrips/calibs${hostname}.html
    echo '
    <table>
    ' >> /root/scrips/calibs${hostname}.html
    cat calibstable.html >> /root/scrips/calibs${hostname}.html
    echo "
    </table>
    </body>
    </html>
    " >> /root/scrips/calibs${hostname}.html

done

