#!/usr/bin/env zsh

# Localhost interface
iptables -I INPUT -i lo -j ACCEPT

# Web interface
iptables -I INPUT -m state --state NEW -p tcp -m tcp --dport 80 -j ACCEPT
iptables -I INPUT -m state --state NEW -p tcp -m tcp --dport 443 -j ACCEPT

# smtp
#iptables -I INPUT -m state --state NEW -p tcp -m tcp --dport 25 -j ACCEPT
#iptables -I INPUT -m state --state NEW -p tcp -m tcp --dport 465 -j ACCEPT

# POS
iptables -I INPUT -m state --state NEW -m tcp -p tcp --match multiport --dports 1900:2000 -j ACCEPT
